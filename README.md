# Coffee finder
## Homepage
- [ ] Navbar
- [ ] Search
- [ ] Suggest
## Coffee Shop
### Coffee detail
- [ ] Photo
- [ ] Info (Name, open/close hours, phone, address,…)
- [ ] Description
### Menu
- [ ] Item …….. Price
### Review
- [ ] User review
- [ ] Form for review
## Promotion
- [ ] Photo
- [ ] Short detail describe about the promotion
- [ ] Promotion code (Font bold style)
- [ ] Show link just link reading new paper
## Hot trend
- [ ] Show top coffee shop base on user rating
- [ ] Blog/News about new coffee, new coffee shop
==> Chose whatever you like
## Search result
- [ ] Whatever you like
- [ ] Result can show in the map (Google map) is high recommened
## About
- [ ] Our team
- [ ] Phone number
- [ ] Address (use embed Google Map)

